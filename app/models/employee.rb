class Employee < ApplicationRecord
    #Kad tiek izveidots lietotājs, tiek arī izveidots attiecīgais darbinieka objekts
    belongs_to :user
    has_many :reservations, dependent: :destroy
    has_many :bicycles, through: :reservations
    validates :name, presence: true
    validates :last_name, presence: true
    validates :email, presence: true
  end
  