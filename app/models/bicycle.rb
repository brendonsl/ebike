class Bicycle < ApplicationRecord
    has_many :reservations, dependent: :destroy
    has_many :employees, through: :reservations
  end
  