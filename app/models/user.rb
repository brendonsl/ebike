class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  
  has_many :reservations, dependent: :destroy # Ja lietotājs tiek dzēsts, tad tiek dzēstas visas rezervācijas
  has_one :employee, dependent: :destroy # Ja lietotājs tiek dzēsts, tad tiek dzēsta arī darbinieks

  after_create :create_employee_record # Pēc lietotāja izveidošanas tiek izveidots arī darbinieks

  private

  def create_employee_record
    self.create_employee(name: self.name, last_name: self.last_name, email: self.email) 
  end
end
