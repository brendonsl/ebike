class Reservation < ApplicationRecord
  belongs_to :employee
  belongs_to :bicycle
  validates :start_time, :end_time, presence: true
  validate :end_time_after_start_time
  validate :no_overlapping_reservations

  scope :expired, -> { where("end_time < ?", Time.current) }

  def self.delete_expired_reservations
    Rails.logger.info "Deleting expired reservations at #{Time.current}"
    expired_reservations = expired
    expired_reservations.each do |reservation|
      Rails.logger.info "Checking reservation #{reservation.id} with end_time #{reservation.end_time}"
    end
    Rails.logger.info "Found #{expired_reservations.count} expired reservations"
    expired_reservations.destroy_all
  end
  private


  def end_time_after_start_time
    if end_time.present? && start_time.present?
      errors.add(:end_time, "Beigu laiks nevar būt pirms sākuma") if end_time <= start_time
    end
  end

  def no_overlapping_reservations
    if Reservation.where(bicycle_id: bicycle_id)
                  .where.not(id: id)
                  .where('start_time < ? AND end_time > ?', end_time, start_time)
                  .exists?
      errors.add(:base, 'Šis velosipēds sajā laika posmā ir jau rezervēts.')
    end
  end
end
