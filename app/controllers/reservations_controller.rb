class ReservationsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_reservation, only: [:edit, :update, :destroy]
  before_action :authorize_user!, only: [:edit, :update, :destroy]
  def index
    @reservations = Reservation.all
    @bicycles = Bicycle.all
    @employees = Employee.all
  end

  def new
    @reservation = Reservation.new
    @bicycles = Bicycle.all
  end

  def create
    @reservation = current_user.employee.reservations.build(reservation_params)
    if @reservation.save
      redirect_to reservations_path, notice: 'Rezervācija izveidota.'
    else
      flash.now[:alert] = @reservation.errors.full_messages.join(", ") #kļūda, ja rezervāciju neizdevās saglabāt
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    @bicycles = Bicycle.all
  end

  def update
    if @reservation.update(reservation_params)
      redirect_to reservations_path, notice: 'Rezervācija atjaunināta.'
    else
      flash.now[:alert] = @reservation.errors.full_messages.join(", ")#kļūda, ja rezervāciju neizdevās rediģēt
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @reservation.destroy
    redirect_to reservations_path, notice: 'Rezervācija izdzēsta.'
  end

  private

  def set_reservation
    @reservation = Reservation.find(params[:id])
  end

  def authorize_user!
    unless @reservation.employee.user == current_user
      redirect_to reservations_path, alert: 'Tu neesi autorizēts lai rediģētu šo rezervāciju.'
    end
  end

  def reservation_params
    params.require(:reservation).permit(:bicycle_id, :start_time, :end_time)
  end
end
