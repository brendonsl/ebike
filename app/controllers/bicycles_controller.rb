class BicyclesController < ApplicationController
  before_action :authenticate_user!
  def index
    @bicycles = Bicycle.all
  end

  def new
    @bicycle = Bicycle.new
  end

  def create
    @bicycle = Bicycle.new(bicycle_params)
    if @bicycle.save
      redirect_to bicycles_path
    else
      render :new
    end
  end

  def edit
    @bicycle = Bicycle.find(params[:id])
  end

  def update

    @bicycle = Bicycle.find(params[:id])
    if @bicycle.update(bicycle_params)
      redirect_to bicycles_path
    else
      render :edit
    end
  end

  def destroy
    @bicycle = Bicycle.find(params[:id])
    @bicycle.destroy
    redirect_to bicycles_path
  end

  def show_reservation
    @bicycle = Bicycle.find(params[:id])
    @reservations = @bicycle.reservations 
    render json: { bicycle: @bicycle, reservations: @reservations }
  end

  private
  def bicycle_params
    params.require(:bicycle).permit(:name)
  end
end
