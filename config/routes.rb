Rails.application.routes.draw do
  get 'pages/index'
  devise_for :users
  root 'pages#index'
  get 'pages/index'
  resources :reservations, only: [:index, :new, :create, :edit, :update, :destroy]
  resources :employees, only: [:index]
  resources :bicycles, only: [:index, :new, :create, :edit, :update, :destroy] 
end
