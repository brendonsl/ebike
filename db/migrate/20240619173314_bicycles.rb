class Bicycles < ActiveRecord::Migration[7.1]
  def change
    create_table :bicycles do |t|
      t.string :name
      t.timestamps
    end
  end
end
