# This file should ensure the existence of records required to run the application in every environment (production,
# development, test). The code here should be idempotent so that it can be executed at any point in every environment.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Example:
#
#   ["Action", "Comedy", "Drama", "Horror"].each do |genre_name|
#     MovieGenre.find_or_create_by!(name: genre_name)
#   end
Bicycle.create(name: 'Gust Ultron 26 zils')
Bicycle.create(name: 'Gust Ultron 26 sarkans')
Bicycle.create(name: 'Skyjet nitro Pro MELNS')
Bicycle.create(name: 'Himo Z20 plus 20 Balts')
Bicycle.create(name: 'Himo Z20 plus 20 Melns')
Bicycle.create(name: 'Himo Z20 plus 20 Sarkans')
Bicycle.create(name: 'Himo Z20 plus 20 Zils')
